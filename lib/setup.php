<?php
// Taxonomies
TaxonomyAssets::local();
TaxonomyAssets::Register();

// Pages
$app->defaultPage('Home', 'home');
$app->defaultPage('Engenho', 'engenho');
$app->defaultPage('Contato', 'contato');

$app->registerTerm('estado', 'ac', 'Acre');
$app->registerTerm('estado', 'ap', 'Amapá');
$app->registerTerm('estado', 'al', 'Alagoas');
$app->registerTerm('estado', 'am', 'Amazonas');
$app->registerTerm('estado', 'ba', 'Bahia');
$app->registerTerm('estado', 'ce', 'Ceará');
$app->registerTerm('estado', 'df', 'Distrito Federal');
$app->registerTerm('estado', 'es', 'Espírito Santo');
$app->registerTerm('estado', 'go', 'Goiás');
$app->registerTerm('estado', 'ma', 'Maranhão');
$app->registerTerm('estado', 'mt', 'Mato Grosso');
$app->registerTerm('estado', 'ms', 'Mato Grosso do Sul');
$app->registerTerm('estado', 'mg', 'Minas Gerais');
$app->registerTerm('estado', 'pa', 'Pará');
$app->registerTerm('estado', 'pb', 'Paraíba');
$app->registerTerm('estado', 'pr', 'Paraná');
$app->registerTerm('estado', 'pe', 'Pernambuco');
$app->registerTerm('estado', 'pi', 'Piauí');
$app->registerTerm('estado', 'rj', 'Rio de Janeiro');
$app->registerTerm('estado', 'rn', 'Rio Grande do Norte');
$app->registerTerm('estado', 'rs', 'Rio Grande do Sul');
$app->registerTerm('estado', 'ro', 'Rondônia');
$app->registerTerm('estado', 'rr', 'Roraima');
$app->registerTerm('estado', 'sc', 'Santa Catarina');
$app->registerTerm('estado', 'sp', 'São Paulo');
$app->registerTerm('estado', 'se', 'Sergipe');
$app->registerTerm('estado', 'to', 'Tocantins');

// Image sizes
//add_image_size('destaque', 1140, 353, true);
add_image_size('post', 320, 217, true);
