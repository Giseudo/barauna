<?php

class TaxonomyAssets {

 private static function app(){
   global $app;
   return $app;
 }

 public static function local(){
   self::app()->registerTaxonomy('estado', 'Estado', 'Estados');
 }

 public static function register(){
   self::app()->registerTaxonomies();
 }
 

}