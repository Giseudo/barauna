<!doctype html>
	<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="pt-BR"> <![endif]-->
	<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="pt-BR"> <![endif]-->
	<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="pt-BR"> <![endif]-->
	<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-BR"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><?php wp_title(); ?></title>
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo('template_url') ?>/favicon.png">
		<link rel="stylesheet" href="<?php bloginfo('template_url') ?>/assets/css/mushi.css">
		<?php wp_head() ?>
	</head>
	<body rel="<?php echo $_pagename ?>">
		<div class="overflow-wrapper">

