<?php
 // Carrega os vendors
	$exploded = explode('-', phpversion());
	$version = (strpos(phpversion(), '-') > -1) ? $exploded[0] : phpversion();

	if($version >= '5.3.2'){
		require "vendor/autoload.php";
	}else{
		error_reporting(E_ERROR);
		foreach(require('vendor/composer/autoload_classmap.php') as $name => $file)
			require_once $file;
		foreach(require('vendor/composer/autoload_files.php') as $file)
			require_once $file;
		require_once 'vendor/twig/twig/lib/Twig/Autoloader.php';
		require_once 'vendor/alterfw/alter/src/main.php';
	}

	require "lib/setup.php";

				require "lib/mail.php";

				if($_POST['send']){
						$statusContato = sendContato();
				}
?>
