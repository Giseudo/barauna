<?php $Home = new WP_Query(array('pagename' => 'Home')); ?>
<?php while($Home->have_posts()): $Home->the_post(); ?>
<section id="home" class="home -item">
	<?php get_template_part('inc/nav', 'menu'); ?>
	<div class="container">
		<div class="post col-xs-12 col-sm-8 col-md-5 col-sm-offset-3">
			<h1><img src="<?php bloginfo('template_url') ?>/assets/img/vai-bem-com-voce.png" alt="Vai bem com você"></h1>
			<?php the_content(); ?>
		</div>
	</div>
</section>
<?php endwhile; ?>