<section id="contato" class="contato">
	<?php get_template_part('inc/nav', 'menu'); ?>
	<div class="container">
		<form action="<?php bloginfo('url'); ?>/#contato" method="post" class="col-xs-12 col-md-9 col-md-offset-3">
			<?php if($_POST['send']): ?>
				<?php isset($statusContato) && $statusContato == 1 ? $mensagem = 'Contato enviado com Sucesso!' : $mensagem = 'Houve um erro ao enviar a mensagem'; ?>
				<div class="alert <?php echo ($statusContato == 1) ? 'alert-success' : 'alert-danger' ; ?>">
					<?php echo $mensagem; ?>
				</div>
			<?php endif; ?>
			<h1>Envie uma mensagem pra gente e nossa<br /> equipe comercial entrará em contato</h1>
			<div class="row">
				<ul class="col-xs-12 col-sm-4">
					<li>
						<input type="text" name="nome" placeholder="Nome">
					</li>
					<li>
						<input type="email" name="email" placeholder="E-mail">
					</li>
					<li>
						<input type="text" name="assunto" placeholder="Assunto">
					</li>
				</ul>
				<ul class="col-xs-12 col-sm-8">
					<li>
						<textarea name="mensagem" id="" cols="30" rows="10"></textarea>
					</li>
					<li>
						<input type="submit" name="send" value="Enviar">
					</li>
				</ul>
			</div>
			<?php $contato = new WP_Query(array('pagename' => 'Contato')); ?>
			<?php while($contato->have_posts()): $contato->the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; ?>
		</form>
	</div>
	<div class="whatsapp">
		<div class="container">
			<div class="col-xs-12 col-sm-7 col-md-5">
				<h1>Informações ou vendas?<br /> Manda um WhatsApp!</h1>
				<ul>
					<li><?php echo get_option('whatsapp1') ?></li>
					<li><?php echo get_option('whatsapp2') ?></li>
					<li><?php echo get_option('whatsapp3') ?></li>
				</ul>
			</div>
		</div>
	</div>
</section>
