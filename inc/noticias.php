<?php $noticias = new WP_Query(array('post_type' => 'post')); ?>
<section id="noticias" class="noticias -item">
	<?php get_template_part('inc/nav', 'menu'); ?>
	<div class="container">
		<div class="-list col-xs-12 col-md-7 col-md-offset-4">
			<div class="swiper-container">
				<div class="swiper-wrapper">

																		<?php while($noticias->have_posts()): $noticias->the_post(); ?>
																		<?php $urlImg = wp_get_attachment_url( get_post_thumbnail_id($noticias->ID) ); ?>
					<article class="swiper-slide">
						<div class="thumb col-xs-12 col-md-6" style="background-image: url(<?php echo $urlImg ?>)"></div>
						<div class="content col-xs-12 col-md-6">
							<time datetime="<?php echo the_time('Y-m-d') ?>"><?php echo the_time('d M Y') ?></time>
							<h1><?php the_title() ?></h1>
																												<p><?php the_excerpt() ?></p>
							<a href="<?php the_permalink() ?>" class="readmore">Leia mais</a>
						</div>
					</article>
																		<?php endwhile; ?>
					<!--article class="swiper-slide">
						<div class="thumb col-xs-12 col-md-6" style="background-image: url(<?php bloginfo('template_url') ?>/assets/img/delete/zelda.jpg)"></div>
						<div class="content col-xs-12 col-md-6">
							<time>01/10/2015</time>
							<h1>Título da notícia aqui</h1>
							<p>Elabora da a partir do calda de canas selecionadas, 100% orgância, possui baixa acidez. O resultado é uma degustação suave, que combina com tudo e vai bem com você.</p>
							<a href="#" class="readmore" title="Leia mais">Leia mais</a>
						</div>
					</article>
					<article class="swiper-slide">
						<div class="thumb col-xs-12 col-md-6" style="background-image: url(<?php bloginfo('template_url') ?>/assets/img/delete/zelda.jpg)"></div>
						<div class="content col-xs-12 col-md-6">
							<time>01/10/2015</time>
							<h1>Título da notícia aqui</h1>
							<p>Elabora da a partir do calda de canas selecionadas, 100% orgância, possui baixa acidez. O resultado é uma degustação suave, que combina com tudo e vai bem com você.</p>
							<a href="#" class="readmore" title="Leia mais">Leia mais</a>
						</div>
					</article>
					<article class="swiper-slide">
						<div class="thumb col-xs-12 col-md-6" style="background-image: url(<?php bloginfo('template_url') ?>/assets/img/delete/zelda.jpg)"></div>
						<div class="content col-xs-12 col-md-6">
							<time>01/10/2015</time>
							<h1>Título da notícia aqui</h1>
							<p>Elabora da a partir do calda de canas selecionadas, 100% orgância, possui baixa acidez. O resultado é uma degustação suave, que combina com tudo e vai bem com você.</p>
							<a href="#" class="readmore" title="Leia mais">Leia mais</a>
						</div>
					</article-->
				</div>
				<button class="prev"></button>
				<button class="next"></button>
			</div>
		</div>
	</div>
</section>
