<div class="intro">
	<div class="container">
		<div class="content">
			<h1 class="logo"><img src="<?php bloginfo('template_url') ?>/assets/img/barauna-cachaca-tradicional.png" alt="Baraúna - Cachaça Tradicional"></h1>
			<div class="center">
				<button class="maior col-xs-6 col-sm-offset-1 col-sm-4">
					<span class="cup -full"></span>
					<p>Sou MAIOR<br /> de 18 anos</p>
				</button>
				<div class="aprecie col-sm-2 hidden-xs">
					<img src="<?php bloginfo('template_url') ?>/assets/img/aprecie.png" alt="Aprecie">
				</div>
				<button class="menor col-xs-6 col-sm-4" disabled="disabled">
					<span class="cup -empty"></span>
					<p>Sou MENOR<br /> de 18 anos</p>
				</button>
				<div class="aprecie col-xs-6 col-xs-offset-3 visible-xs">
					<img src="<?php bloginfo('template_url') ?>/assets/img/aprecie.png" alt="Aprecie">
				</div>
			</div>
		</div>
	</div>
</div>
