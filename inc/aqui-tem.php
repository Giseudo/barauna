<?php
		$cats = get_terms('estado',array('hide_empty' => false));
?>

<section id="aqui-tem" class="aqui-tem -item">
	<?php get_template_part('inc/nav', 'menu'); ?>
	<div class="container">
		<div class="title col-xs-12 col-sm-4 col-md-offset-2">
			<h1><img src="<?php bloginfo('template_url') ?>/assets/img/aqui-tem-barauna.png" alt="Aqui tem Baraúna"></h1>
		</div>
		<div class="mapa col-xs-12 col-sm-6 hidden-xs"></div>
		<div class="state col-xs-12 visible-xs">
			<select name="state" id="state">
												<?php
														foreach($cats as $cat): ?>
														<option value="<?php echo $cat->slug; ?>" <?php echo ($cat->slug == 'pb') ? 'selected="selected"' : '' ; ?>><?php echo $cat->name; ?></option>
												<?php endforeach; ?>
			</select>
		</div>
								<?php
								$estadosBrasileiros = array(
										'AC'=>'Acre',
										'AL'=>'Alagoas',
										'AP'=>'Amapá',
										'AM'=>'Amazonas',
										'BA'=>'Bahia',
										'CE'=>'Ceará',
										'DF'=>'Distrito Federal',
										'ES'=>'Espírito Santo',
										'GO'=>'Goiás',
										'MA'=>'Maranhão',
										'MT'=>'Mato Grosso',
										'MS'=>'Mato Grosso do Sul',
										'MG'=>'Minas Gerais',
										'PA'=>'Pará',
										'PB'=>'Paraíba',
										'PR'=>'Paraná',
										'PE'=>'Pernambuco',
										'PI'=>'Piauí',
										'RJ'=>'Rio de Janeiro',
										'RN'=>'Rio Grande do Norte',
										'RS'=>'Rio Grande do Sul',
										'RO'=>'Rondônia',
										'RR'=>'Roraima',
										'SC'=>'Santa Catarina',
										'SP'=>'São Paulo',
										'SE'=>'Sergipe',
										'TO'=>'Tocantins'
								);
								foreach($cats as $cat): ?>
								<div id="<?php echo $cat->slug; ?>" class="-list <?php if($cat->slug == 'pb'){ ?> -active <?php } ?> col-xs-12 col-md-8 col-md-offset-4">
														<h1><?php echo $cat->name; ?></h1>
														<ul>
																<?php
																		$wpq = array ('taxonomy'=>'estado','term' => $cat->slug, 'posts_per_page' => 99, 'limit' => 99);
																		$myquery = new WP_Query ($wpq);
																		while ($myquery->have_posts()) : $myquery->the_post();
																?>
																		<li class="col-xs-12 col-sm-6 col-md-4"><?php echo $post->post_title; ?></li>
																		<!--li class="col-xs-12 col-sm-6 col-md-4">Supermercado Manaíra</li>
																		<li class="col-xs-12 col-sm-6 col-md-4">Supermercado Manaíra</li>
																		<li class="col-xs-12 col-sm-6 col-md-4">Superbox Brasil</li>
																		<li class="col-xs-12 col-sm-6 col-md-4">Superbox Brasil</li>
																		<li class="col-xs-12 col-sm-6 col-md-4">Superbox Brasil</li>
																		<li class="col-xs-12 col-sm-6 col-md-4">Carrefour</li>
																		<li class="col-xs-12 col-sm-6 col-md-4">Carrefour</li>
																		<li class="col-xs-12 col-sm-6 col-md-4">Carrefour</li-->
																		<?php endwhile; ?>
														</ul>
										</div>
								<?php endforeach; ?>
<!--		<div id="pb" class="-list -active col-xs-12 col-md-8 col-md-offset-4">
			<h1>Paraíba</h1>
			<ul>
				<li class="col-xs-12 col-sm-6 col-md-4">Supermercado Manaíra</li>
				<li class="col-xs-12 col-sm-6 col-md-4">Supermercado Manaíra</li>
				<li class="col-xs-12 col-sm-6 col-md-4">Superbox Brasil</li>
				<li class="col-xs-12 col-sm-6 col-md-4">Superbox Brasil</li>
				<li class="col-xs-12 col-sm-6 col-md-4">Carrefour</li>
				<li class="col-xs-12 col-sm-6 col-md-4">Carrefour</li>
			</ul>
		</div>-->
	</div>
	<?php include('contato.php'); ?>
</section>
