<header class="-mobile visible-xs">
	<h1 class="logo">
		<img src="<?php bloginfo('template_url') ?>/assets/img/barauna-cachaca-tradicional.png" alt="Baraúna - Cachaça Regional">
	</h1>
	<button class="menu-btn glyphicon glyphicon-menu-hamburger"></button>
	<nav>
		<img src="<?php bloginfo('template_url') ?>/assets/img/barauna-bottle.png" class="bottle" alt="Garrafa - Baraúna">
		<button class="close-btn">X</button>
		<a href="#home">Baraúna</a>
		<a href="#engenho">Engenho</a>
		<a href="#noticias">Notícias</a>
		<a href="#aqui-tem">Aqui tem</a>
		<a href="#contato">Contato</a>
	</nav>
</header>
