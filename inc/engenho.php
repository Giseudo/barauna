<?php $Engenho = new WP_Query(array('pagename' => 'Engenho')); ?>
<?php while($Engenho->have_posts()): $Engenho->the_post(); ?>
<section id="engenho" class="engenho -item">
	<?php get_template_part('inc/nav', 'menu'); ?>
	<?php
		$engenho_img_1 = wp_get_attachment_image_src (get_post_meta(get_the_ID(), 'engenho_img_1', true), 'default')[0];
		$engenho_img_2 = wp_get_attachment_image_src (get_post_meta(get_the_ID(), 'engenho_img_2', true), 'default')[0];
	?>
	<div class="container">
		<div class="col-xs-12 col-md-8 col-md-offset-3">
			<div class="row">
				<div class="col-xs-12 col-md-4 visible-xs">
					<a href="<?php echo $engenho_img_1 ?>" class="square" style="<?php echo (!empty($engenho_img_1)) ? 'background-image: url('. $engenho_img_1 . ')' : '' ; ?>"></a>
					<a href="<?php echo $engenho_img_2 ?>" class="square" style="<?php echo (!empty($engenho_img_2)) ? 'background-image: url('. $engenho_img_2 . ')' : '' ; ?>"></a>
				</div>
				<div class="post col-xs-12 col-md-8">
					<?php the_content(); ?>
				</div>
				<div class="col-xs-12 col-md-4 hidden-xs">
					<a href="<?php echo $engenho_img_1 ?>" class="square" style="<?php echo (!empty($engenho_img_1)) ? 'background-image: url('. $engenho_img_1 . ')' : '' ; ?>"></a>
					<a href="<?php echo $engenho_img_2 ?>" class="square" style="<?php echo (!empty($engenho_img_2)) ? 'background-image: url('. $engenho_img_2 . ')' : '' ; ?>"></a>
				</div>
			</div>
		</div>
	</div>
</section>
<?php endwhile; ?>
