<style type="text/css">
	body{
		font-family: Helvetica, Arial;
	}
	.alignleft{
		float: left;
		margin-right: 10px;
		margin-bottom: 10px;
	}
	.alignright{
		float: right;
		margin-left: 10px;
		margin-bottom: 10px;
	}
	.aligncenter{
		display: table;
		margin-left: auto;
		margin-right: auto;
	}
	p{
		font-size: 85%;
		line-height: 150%;
	}
</style>
<?php while(have_posts()): the_post(); ?>
<?php $urlImg = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) ); ?>
<div class="post">
				<h1><?php the_title() ?></h1>
	<?php the_content() ;?>
</div>
<?php endwhile; ?>
