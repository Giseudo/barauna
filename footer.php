			<footer class="rodape">
				<nav class="social">
					<a href="<?php echo get_option('facebook') ?>" class="icon icon-facebook"></a>
					<a href="<?php echo get_option('twitter') ?>" class="icon icon-twitter"></a>
					<a href="<?php echo get_option('linkedin') ?>" class="icon icon-linkedin"></a>
					<a href="<?php echo get_option('rss') ?>" class="icon icon-rss"></a>
				</nav>
			</footer>

			<script type="text/javascript" src="<?php bloginfo('template_url') ?>/assets/js/mushi.js"></script>
			<span class="overlay-all"></span>
		</div>
	</body>
	<?php wp_footer() ?>
</html>
