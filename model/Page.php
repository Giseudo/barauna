<?php
class Page extends AppModel{
	public $fields = array(
		'engenho_img_1' => array(
			'label' => 'Imagem Engenho 01',
			'type'      => 'image'
		),
		'engenho_img_2' => array(
			'label' => 'Imagem Engenho 02',
			'type'      => 'image'
		)
	);
}
