<?php
class Local extends AppModel{

 public $route = 'local';
 public $singular = 'Local';
 public $plural = 'Locais';

 public $taxonomies = array('estado');

 public $icon = 'dashicons-star-filled';

 public $fields = array(
	 'title' => true
 );

}
