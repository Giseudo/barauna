/*global $, Mushi, console, _, alert, Swiper */

Mushi.common = {
	init: function () {
		console.log('Common started');
		this.introEnter();
		this.loadMap();
		this.mobileMenu();
		this.scrollMenu();
		this.mobileScrollMenu();
		this.validateContato('section.contato form');
		this.bottleLimit();
		this.fancyEngenho('section.engenho a.square');
		this.fancyNews('section.noticias article a');
		this.swiperNews('section.noticias .swiper-container');
		this.phoneMask('input[name="telefone"]');
		this.mobileAquiTem();
	},

	activeElement: function (target, className) {
		$(target).addClass(className);
	},

	deactiveElement: function (target, className) {
		$(target).removeClass(className);
	},

	closeAll: function () {
		Mushi.common.deactiveElement('header.-mobile nav', '-active');
		Mushi.common.deactiveElement('span.overlay-all', '-active');
	},

	introEnter: function () {
		if (window.location.hash) {
			$('.intro').fadeOut();
		} else {
			$('.intro button.maior').bind('click', function () {
				$('.intro').fadeOut();
				return false;
			});
		}
	},

	loadMap: function () {
		$('.mapa').vectorMap({
			map: 'brazil_br',
			backgroundColor: null,
			borderColor: '#513727',
			borderOpacity: 1,
			borderWidth: 1,
			color: 'transparent',
			enableZoom: false,
			hoverColor: 'rgba(81, 55, 39, 0.7)',
			hoverOpacity: 0,
			normalizeFunction: 'linear',
			selectedColor: '#513727',
			selectedRegion: 'PB',
			showTooltip: true,
			onRegionClick: function (element, code, region) {
				Mushi.common.mapClick(element, code, region);
				//var message = 'You clicked "' + region + '" which has the code: ' + code.toUpperCase();
				//alert(message);
			},
			onRegionOver: function (element, code, region) {
				if ($(window).width() <= 1024) {
					Mushi.common.mapClick(element, code, region);
				}
			}
		});
	},

	mapClick : function (_element, _code, _region) {
		Mushi.common.deactiveElement('.aqui-tem .-list', '-active');
		Mushi.common.activeElement('.aqui-tem .-list#' + _code.toLowerCase(), '-active');
	},

	mobileMenu : function () {
		$('header.-mobile button.menu-btn').bind('click', function () {
			Mushi.common.activeElement('header.-mobile nav', '-active');
			Mushi.common.activeElement('span.overlay-all', '-active');
			return false;
		});
		this.mobileMenuClose();
	},

	scrollMenu : function () {
		$('nav.menu a').bind('click', function () {
			var target = $(this).attr('href');
			$('body, html').stop(false, false).animate({ scrollTop : $(target).offset().top + $(target).innerHeight() - $(target).height() }, 1000);
			return false;
		});
	},

	mobileScrollMenu : function () {
		$('header.-mobile nav a').bind('click', function () {
			var target = $(this).attr('href');
			$('body, html').stop(false, false).animate({ scrollTop : $(target).offset().top + $(target).innerHeight() - $(target).height() - $('header.-mobile').height() }, 1000);
			Mushi.common.closeAll();
			return false;
		});
	},

	mobileMenuClose : function () {
		$('header.-mobile button.close-btn, span.overlay-all').bind('click', function () {
			Mushi.common.closeAll();
			return false;
		});
	},

	mobileAquiTem: function () {
		$('#aqui-tem select').bind('change', function () {
			var val = $(this).val();
			Mushi.common.deactiveElement('.aqui-tem .-list', '-active');
			Mushi.common.activeElement('.aqui-tem .-list#' + val, '-active');
		});
	},

	validateContato: function (target) {
		var options = {
			errorElement : 'em',
			rules : {
				nome: "required",
				mensagem: "required",
				email : {
					required : true,
					email : true
				}
			},
			messages : {
				nome: "Preencha o seu nome",
				email: "Preencha o seu e-mail",
				mensagem: "Preencha a sua mensagem"
			}
		};
		$(target).validate(options);
	},

	bottleLimit: function () {
		$(window).bind('scroll', function () {
			if ($('body').scrollTop() >= $('footer.rodape').offset().top - 500) {
				$('.barauna-bottle').css('top', $('.barauna-bottle').offset().top);
				$('.barauna-bottle').addClass('-fixed');
			} else {
				$('.barauna-bottle').css('top', '');
				$('.barauna-bottle').removeClass('-fixed');
			}
		});
	},

	fancyEngenho: function (target) {
		$(target).fancybox({
			overlayShow : true,
			scrolling : 'yes'
		});
	},

	fancyNews: function (target) {
		$(target).fancybox({
			overlayShow : true,
			scrolling : 'yes',
			width: 600,
			type : 'iframe'
		});
	},

	swiperNews: function (target) {
		var options = {
			autoplay: 0,
			loop: false,
			slidesPerView: 1,
			slidesPerColumn: 3,
			spaceBetween: 30,
			autoplayDisableOnInteraction: false,
			prevButton: target + ' button.prev',
			nextButton: target + ' button.next',
			onInit: function () {

			}
		}, swiper = new Swiper(target, options);
	},

	phoneMask : function (target) {
		var SPMaskBehavior = function (val) {
				return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
			},
			spOptions = {
				onKeyPress: function (val, e, field, options) {
					field.mask(SPMaskBehavior.apply({}, arguments), options);
				}
			};

		$(target).mask(SPMaskBehavior, spOptions);
	}
};
