<?php $_pagename = 'home'; include "header.php"; ?>

<?php get_template_part('inc/intro'); ?>

<div class="main">
	<?php get_template_part('inc/nav', 'mobile'); ?>
	<div class="barauna-bottle">
		<div class="container">
			<div class="col-xs-3 hidden-xs">
				<img src="<?php bloginfo('template_url') ?>/assets/img/barauna-bottle.png" alt="Garrafa - Baraúna">
			</div>
		</div>
	</div>

	<?php get_template_part('inc/home'); ?>
	<?php get_template_part('inc/engenho'); ?>
	<?php get_template_part('inc/noticias'); ?>
	<?php include('inc/aqui-tem.php'); ?>
</div>

<?php get_footer() ?>
