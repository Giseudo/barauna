<?php

class Contato extends OptionPage{

 public $title = 'Informações de Contato';
 public $capability = 'switch_themes';

 public $fields = array(

	 'email' => array(
		 'label' => 'Email',
		 'type' => 'text'
	 ),

	 'twitter' => array(
		 'label' => 'Twitter',
		 'type' => 'text'
	 ),

	 'facebook' => array(
		 'label' => 'Facebook',
		 'type' => 'text'
	 ),

	 'flickr' => array(
		 'label' => 'Flickr',
		 'type' => 'text'
	 ),

	 'linkedin' => array(
		 'label' => 'LinkedIN',
		 'type' => 'text'
	 ),

	 'rss' => array(
		 'label' => 'RSS',
		 'type' => 'text'
	 ),

	 'whatsapp1' => array(
		 'label' => 'Whatsapp (1)',
		 'type' => 'text'
	 ),

	 'whatsapp2' => array(
		 'label' => 'Whatsapp (2)',
		 'type' => 'text'
	 ),

	 'whatsapp3' => array(
		 'label' => 'Whatsapp (3)',
		 'type' => 'text'
	 )

 );

}
